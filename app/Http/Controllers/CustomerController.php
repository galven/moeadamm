<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $id = Auth::id(); // 
        $customers = Customer::all();
     //   $user =User::find($id);
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  if (Gate::denies('manager','salesrep')) {
       //     abort(403,"You are not allowed to add to the customers list");
     //  } 
        $customer = new Customer();
        $id= Auth::id();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->status = 0;
        $customer->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $customers = $user->customers;
        $customers = $customer->where("user_id", "=", $user->id)->get();
    
    
        return view('admin.post.show', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  if (Gate::denies('manager','salesrep')) {
      //      abort(403,"You are not allowed to edit the customers list");
    //   } 
        $customer = Customer::find($id);
        return view('customers.edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
     if (Gate::denies('manager')){   // בדיקה שמנהל הוא זה שרוצה לערוך
       $customer->update($request -> all());
         }
    
     if (Gate::allows('salesrep')){ 
             if(!$customer->user->id == Auth::id()) abort(403,"Sorry, you do not have premission to edit this customer!"); 
        $customer->update($request -> all());
        }
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     if (Gate::denies('manager')) {
        abort(403,"You are not allowed to delete customer");
   } 
        $customer = Customer::find($id);
        $customer->delete(); 
        return redirect('customers');
    }

    public function close($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to mark customers");
         }          
        $customer = Customer::findOrFail($id);            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }

    public function indexFiltered()
    {
        $id = Auth::id(); //the current user 
        $customers = Customer::where('user_id', $id)->get();
        $filtered = 1; //this is to mark the view to show link to all tasks 
        return view('customers.index', compact('customers','filtered'));
    }
       

}
