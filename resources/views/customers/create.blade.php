<html>
@yield('content')
@extends('layouts.app')
@section('content') 
<h1> Create a new customer </h1>

<form method = 'post' action = "{{action('CustomerController@store')}}">
{{csrf_field()}}

<div class="form-group">
<label for = "title">Name</label>
<input type = "text" class = "form-control" name= "name">
<label for = "title">Email</label>
<input type = "text" class = "form-control" name= "email">
<label for = "title">Phone</label>
<input type = "integer" class = "form-control" name= "phone">
</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>
</form>

@endsection
</html>