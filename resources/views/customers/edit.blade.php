<h1> Edit your Customer</h1>
@extends('layouts.app')
@section('content') 
<form method = 'post' action = "{{action('CustomerController@update', $customer ->id)}}">
@csrf
@method('PUT')

<div class="form-group">
<label for = "title">Update the name of the customer </label>
<input type = "text" class = "form-control" name= "name" value = "{{$customer->name}}">
<label for = "title"> Update the email of the customer </label>
<input type = "text" class = "form-control" name= "email" value = "{{$customer->email}}">
<label for = "title"> Update the phone of the customer </label>
<input type = "integer" class = "form-control" name= "phone" value = "{{$customer->phone}}">
</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>
</form>
@endsection