<!DOCTYPE html>
<html>
@yield('content')
@extends('layouts.app')
@section('content')
@isset($filtered)
     <a href = "{{route('customers.index')}}">All Customers</a>
    @else
     <a href = "{{route('myfilter')}}">My customers</a>
    @endisset
<head><h3>This is your Customers list</h3></head>
<body>
<a href = "{{route('customers.create')}}"> create a new Customer </a>
<table>
    <tr>
     <th>Id</th>
     <th>Name</th>
     <th>Email</th>
     <th>Phone</th>
     <th>Name of the User who created the customer</th>
    </tr>

    @foreach($customers as $customer)
    <tr> 
      <td>{{$customer->id}}</td>
      <td> {{$customer->name}} </a></td> 
      <td> {{$customer->email}} </a></td>
      <td> {{$customer->phone}} </a></td>
      <td> {{$customer->user->name}} </a> </td>
      <td> <a href="{{route('customers.edit', $customer->id)}}">Edit</a></td>
      <td>  @can('manager') <a href="{{route('delete', $customer->id)}}"> @endcan Delete</a></td>
      <td>   @if ($customer->status == 0)
      @can('manager') <a href="{{route('close', $customer->id)}}">Deal Close</a></td> @endcan
             @else
             Done!
             @endif
   @endforeach
</table>
@endsection
</html>
